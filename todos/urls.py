from django.urls import path
from .views import show_todo_list, todo_list_info, todo_list_create, todo_list_update

urlpatterns = [
    path("", show_todo_list, name="todo_list_list"),
    path("<int:id>/", todo_list_info, name='todo_list_info'),
    path("create/", todo_list_create, name='todo_list_create'),
    path("<int:id>/edit/", todo_list_update, name='todo_list_update'),
]
