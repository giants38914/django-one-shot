from django.forms import ModelForm
from .models import TodoList


# creating a form
class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            'name'
        ]

class TodoList(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            'name'
        ]
