from django.shortcuts import render, redirect
from .models import TodoList
from .forms import TodoListForm

# Create your views here.


def show_todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list}
    return render(request, "todos/list.html", context)

def todo_list_info(request, id):
    todo_list = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": todo_list}
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_info", model_instance.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)
      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)

def update_model_name(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    form = ModelForm(request.POST, instance=model_instance)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      model_instance = form.save()
      return redirect("detail.html", id=model_instance.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoList(instance=model_instance)

  context = {
    "form": form
  }

  return render(request, "model_names/edit.html", context)
